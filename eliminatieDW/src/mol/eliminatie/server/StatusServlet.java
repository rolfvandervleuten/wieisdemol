package mol.eliminatie.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mol.eliminatie.ElStatus;

public class StatusServlet extends HttpServlet {
	private int elState = 4;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println(this.elState);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String set = req.getParameter("set");
		if(set != null){
			try{this.elState = Integer.parseInt(set);}catch(NumberFormatException e){};
		}
		
		
	}
}
