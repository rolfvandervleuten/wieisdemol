<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ page import="mol.eliminatie.ElStatus" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<title>Eliminatie controller</title>

	<script src="jquery-1.9.1.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">

	<script type="text/javascript" >
		function setStatus(aState){
			status = $.post('status', {set : aState});
		}
		
	</script>
</head>
<body>
	<br/><br/><h1>De grote geheime <br/>Geert controller</h1><br/><br/>
	<div class="button button_stay" onClick="setStatus('1');"></div>
	<div class="button button_go" onClick="setStatus('2');"></div>
	<div class="button button_flash" onClick="setStatus('0');"></div>
	<div class="button button_white" onClick="setStatus('4');"></div>
</body>
</html>