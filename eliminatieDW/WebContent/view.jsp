<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<%@ page import="mol.eliminatie.ElStatus" %>     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>Eliminatie view</title>
<script src="jquery-1.9.1.js"></script>

<script type="text/javascript">
	setInterval(function () {
		updateStatus();
	}, 500);
	 
	var color = '#ff0000';
	function updateStatus(){
		$.get('status', function(data){
			
			console.log(data);
			
			if(data == 1){
				$("body").css('background-color','#00ff00');
			}	
			if(data == 2){	
				$("body").css('background-color','#ff0000');
			}
			if(data == 0){
				if(color == '#ff0000'){
					$("body").css('background-color','#00ff00');
				 	color = '#00ff00';
				}
				else{
					$("body").css('background-color','#ff0000');
					 color = '#ff0000'; 
				}
			}
			if(data == 4){	
				$("body").css('background-color','#ffffff');
			}			
		});
	}
</script>

</head>
<body >
	<img src="Fingerprint_klein.gif" style="float:right; margin-right:40px; margin-top:520px; opacity: 0.2" id="img_finger"/>
</body>
</html>